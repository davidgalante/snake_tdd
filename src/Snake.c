/**
 * The Snake.c file defines a game actor that has a 50x40 size image built 
 * from the "snake.png" file. The actSnake() method declared in the Snake.c  
 * file defines the snake behaviour in each cycle of the scenario execution.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

/**
 * Initialize its image.
 */
void startSnake(Actor snake) {
	setImageFile(snake, "black_snake.png"); //Test 12
	setImageScale(snake, 50, 50); //Test 15 y 16
	//Test 7
	setGlobalInt(snake, "angle", 0);
	//Test 9
    setGlobalInt(snake, "distance", 2);

}

void actSnake(Actor snake) { //Test 19 y 20
    turn(snake, getGlobalInt(snake, "angle"));
    move(snake, 2);
}

void setAngle(Actor snake, int angle) { //Test 8
    setGlobalInt(snake, "angle", angle);
}

void setDistance(Actor snake, int distance) { //Test 10
    setGlobalInt(snake, "distance", distance);
}
