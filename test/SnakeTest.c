/**
 * Tests of Snake. 
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "SnakeWorld.c"
#include "Snake.c"

void testImage() { //Test 11
    // Given
    Actor snake = newActor("Snake");

	// When
	const char* imageFile = getImageFile(snake);

	// Then
	assertEquals_String("black_snake.png", imageFile);
}

void testImageWidth() { //Test 13
    Actor snake = newActor("Snake");

    // When
    int width = getImageWidth(snake);
        
    // Then
	assertEquals_int(50, width);
}

void testImageHeight() { //Test 14
    Actor snake = newActor("Snake");

    // When
    int height = getImageHeight(snake);
        
    // Then
	assertEquals_int(50, height);
}

void testAct_X() { //Test 17
    // Given
    World snakeWorld = newWorld("SnakeWorld");
    Actor snake = getSnake(snakeWorld);
    // When
    actSnake(snake);
    // Then
    assertEquals_int(4, getX(snake));
}

void testAct_Y() { //Test 18
    // Given
    World snakeWorld = newWorld("SnakeWorld");
    Actor snake = getSnake(snakeWorld);
    // When
    actSnake(snake);
    // Then
    assertEquals_int(3, getY(snake));
}

void testActAfterSetDistance_X() { //Test 21
    // Given
    World snakeWorld = newWorld("SnakeWorld");
    Actor snake = getSnake(snakeWorld);
    setDistance(snake, 3);
    // When
    actSnake(snake);
    // Then
    assertEquals_int(5, getX(snake));
}
    
void testActAfterSetDistance_Y() { //Test 22
    // Given
    World snakeWorld = newWorld("SnakeWorld");
    Actor snake = getSnake(snakeWorld);
    setDistance(snake, 3);
    // When
    actSnake(snake);
    // Then
    assertEquals_int(3, getY(snake));
}
    
void testActAfterSetAngle_X() { //Test 25
    // Given
    World snakeWorld = newWorld("SnakeWorld");
    Actor snake = getSnake(snakeWorld);
    setAngle(snake, 1);
    // When
    actSnake(snake);
    // Then
    assertEquals_int(4, getX(snake));
}
    
void testActAfterSetAngle_Y() { //Test 26
    // Given
    World snakeWorld = newWorld("SnakeWorld");
    Actor snake = getSnake(snakeWorld);
    setAngle(snake, 1);
    // When
    actSnake(snake);
    // Then
    assertEquals_int(3, getY(snake));
}
    
void testActAfterSetDistanceAngle_X() { //Test 29
    // Given
    World snakeWorld = newWorld("SnakeWorld");
    Actor snake = getSnake(snakeWorld);
    setAngle(snake, 2);
    setDistance(snake, 4);
    // When
    actSnake(snake);
    // Then
    assertEquals_int(6, getX(snake));
}

void testActAfterSetDistanceAngle_Y() { //Test 30
    // Given
    World snakeWorld = newWorld("SnakeWorld");
    Actor snake = getSnake(snakeWorld);
    setAngle(snake, 2);
    setDistance(snake, 4);
    // When
    actSnake(snake);
    // Then
    assertEquals_int(3, getY(snake));
}
