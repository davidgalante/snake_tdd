/**
 * Tests of SnakeWorld. 
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "SnakeWorld.c"


void testGetSnake_X() { //Test3
    //Given
    World snakeWorld = newWorld("SnakeWorld");
    Actor snake = getSnake(snakeWorld);
    // When
    int x = getX(snake);
    // Then
    assertEquals_int(2, x);
}

void testGetSnake_Y() {//Test4
    //Given
    World snakeWorld = newWorld("SnakeWorld");
    Actor snake = getSnake(snakeWorld);
    // When
    int y = getY(snake);
    // Then
    assertEquals_int(3, y);
}
